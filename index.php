<?php

    session_start();

    require_once './src/config/config.php';

    function wunder_autoloader($class) {
        require_once './src/' . str_replace('\\', '/', $class) . '.php';
    }

    use \Controller\Step1;
    use \lib\Sqlite;

    spl_autoload_register('wunder_autoloader');

    $instance = Sqlite::getInstance();
    $conn = $instance->getConnection();

    // Handle post when is needed
    if ($_POST) {
        $stepClass = "\Controller\\" . $_REQUEST['step'];
        $step = new $stepClass($conn);
        $step->handlePost($_POST);
    }

    // Go to next steps
    if (isset($_REQUEST['step'], $_SESSION['customerId'])
        && $_REQUEST['step'] !== '' &&
        $_SESSION['customerId'] > 0 &&
        class_exists("\Controller\\" . $_REQUEST['step'])
    ) {
        $stepClass = "\Controller\\" . $_REQUEST['step'];
        $step = new $stepClass($conn);
    } else {
        $step = new Step1($conn);
        unset($_SESSION['customerId']);
    }

    // Output the informations
    require_once './src/View/layout/layout.php';

CREATE TABLE "customer" (
	"id"	INTEGER,
	"firstname"	varchar DEFAULT 55,
	"lastname"	varchar DEFAULT 55,
	"telephone"	vachar DEFAULT 55,
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE "address" (
	"id"	INTEGER,
	"customerId"	INTEGER,
	"street"	varchar DEFAULT 100,
	"number"	varchar DEFAULT 20,
	"zipcode"	varchar DEFAULT 100,
	"city"	varchar DEFAULT 100,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("customerId") REFERENCES "customer"("id")
);

CREATE TABLE "data_payment" (
	"id"	INTEGER,
	"customerId"	INTEGER,
	"owner"	varchar DEFAULT 55,
	"iban"	varchar DEFAULT 55,
	"dataPaymentId"	varchar DEFAULT 255,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("customerId") REFERENCES "customer"("id")
);
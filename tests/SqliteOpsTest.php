<?php

require_once './src/config/config.php';
require_once './src/lib/Sqlite.php';
require_once './src/lib/SqliteOps.php';

use lib\Sqlite;
use lib\SqliteOps;
use PHPUnit\Framework\TestCase;

class SqliteOpsTest extends TestCase
{
    private $conn;

    public function setUp():void
    {
        $instance = Sqlite::getInstance();
        $this->conn = $instance->getConnection();
    }

    public function testHandleStep1()
    {
        $tableName = 'customer';
        $parameters = [
            "firstname" => "Test",
            "lastname" => "Test",
            "telephone" => "0720000000"
        ];
        $sqliteOps = new SqliteOps($this->conn);
        $id = $sqliteOps->insert($tableName, $parameters);

        $this->assertIsInt($id);
    }

    public function testHandleStep2()
    {
        $tableName = 'address';
        $parameters = [
            "customerId" => 1,
            "street" => "Test",
            "number" => "55",
            "zipcode" => "95001",
            "city" => "Test"
        ];
        $sqliteOps = new SqliteOps($this->conn);
        $id = $sqliteOps->insert($tableName, $parameters);

        $this->assertIsInt($id);
    }

    public function testHandleStep3()
    {
        $tableName = 'data_payment';
        $parameters = [
            "customerId" => 1,
            "owner" => "Test Test",
            "iban" => "T012345",
            "dataPaymentId" => "34f54af59ceb280b0170cbf5c3066b6fb5b50b04f2c9ba53fccd3affc1abe3ac10188e977b36a268d8d28d7f9d670515"
        ];
        $sqliteOps = new SqliteOps($this->conn);
        $id = $sqliteOps->insert($tableName, $parameters);

        $this->assertIsInt($id);
    }
}

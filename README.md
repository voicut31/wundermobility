# Wunder Mobility Test 
@author: Voicu Tibea

## Description
Implement the backend aplication test (details in `Wunder Backend take home test.pdf`) 

## Implementation
- `src` is the folder with the implemented files;
- database is stored in `database\vm.sqlite` (an ORM like Doctrine can be used for database interaction);
- database query tocreate tables is in `database\wm.sql`;
- singleton patern used for database class;
- a pattern used to load steps classes;
- php autoloader is used to load classes;
- namespaces are used;
- abstract class implemented to be extended by steps classes;
- a trait is used in controller classes;
- dependency injection is used;
- Bootstrap CSS Framework  is used for a nice view;
- a progress bar was added for the progress visibility;
- PHPDoc annotation used;
- for template rendering plain php is used (twig, smarty or other template engine can be used)

## Requirements
- PHP 7
- SQLite extension
- Curl extension
- PhPunit (for unittests, teste with phpunit v.8.2.5)

## What to improve
- return fields with errors from php to be displayed in form;
- keep the forms values;
- use Doctrine as database manager;
- use Twig as template system;
- add phphunit configuration file;
- extend UnitTests

### Use the api-platform to build the app
I would like to use api-platform (https://api-platform.com/) to build a backend platform and implement a React JS application over the backend app; 

## Running
- `php -S localhost:8081` or under a server (Apache, nginx, a.o.)
- run the unit test from terminal: `phpunit tests/SqliteOpsTest.php`

## Validation
### Client Side
- a `required` parameter was added for every required input 
### Server side
- server side validation in PHP

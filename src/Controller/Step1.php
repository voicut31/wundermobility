<?php

namespace Controller;

require_once 'WunderTrait.php';

use \lib\SqliteOps;

/**
 * Class Step1
 * @package Controller
 */
class Step1 extends Controller
{
    use WunderTrait;

    /**
     * @var \lib\Sqlite
     */
    private $conn;

    /**
     * @var string
     */
    private $view = 'step1';

    /**
     * @var string
     */
    private $name = 'Step 1';

    /**
     * @var int
     */
    private $progress = 25;

    /**
     * @var string
     */
    private $tableName = 'customer';

    /**
     * Step1 constructor.
     * @param $conn
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param $parameters
     * @return mixed|void
     */
    public function handlePost($parameters)
    {
        $params = [
            'firstname' => $parameters['firstname'],
            'lastname' => $parameters['lastname'],
            'telephone' => $parameters['telephone']
        ];
        if (!$this->validateParams($params)) {
            $_SESSION['error'] = "There was a problem validation fields";
            return false;
        }

        $sqlite = new SqliteOps($this->conn);
        $customerId = $sqlite->insert($this->tableName, $params);

        // Save the customer id in session for later use
        $_SESSION['customerId'] = $customerId;

        header("Location: index.php?step=Step2");
    }

    /**
     * @param array $params
     * @return bool
     */
    public function validateParams(array $params) :bool
    {
        foreach($params as $i => $v) {
            if (empty($v)) {
                return false;
            }
        }

        return true;
    }
}
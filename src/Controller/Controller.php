<?php

namespace Controller;

/**
 * Class Controller - an abstract class to force method definition in child classes
 */
abstract class Controller
{
    /**
     * @return mixed
     */
    abstract public function handlePost($parameters);

    /**
     * @return mixed
     */
    abstract public function getView();

    /**
     * @return mixed
     */
    abstract public function getName();

    /**
     * @return mixed
     */
    abstract public function getProgress();

}
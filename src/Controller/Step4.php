<?php

namespace Controller;

require_once 'WunderTrait.php';

use Controller\Controller;

class Step4 extends Controller
{
    use WunderTrait;

    /**
     * @var string
     */
    private $view = 'step4';

    /**
     * @var string
     */
    private $name = 'Step 4';

    /**
     * @var string
     */
    private $nextStep = 'Step4';

    /**
     * @var int
     */
    private $progress = 100;

    /**
     * @param $parameters
     * @return mixed|void
     */
    public function handlePost($parameters) {
        return false;
    }
}

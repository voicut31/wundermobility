<?php

namespace Controller;

use lib\SqliteOps;

require_once 'WunderTrait.php';

/**
 * Class Step2
 * @package Controller
 */
class Step2 extends Controller
{
    use WunderTrait;

    /**
     * @var \lib\Sqlite
     */
    private $conn;

    /**
     * @var string
     */
    private $view = 'step2';

    /**
     * @var string
     */
    private $name = 'Step 2';

    /**
     * @var int
     */
    private $progress = 50;

    /**
     * @var string
     */
    private $tableName = 'address';

    /**
     * Step2 constructor.
     * @param $conn
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param $parameters
     * @return mixed|void
     */
    public function handlePost($parameters)
    {
        $params = [
            'customerId' => $_SESSION['customerId'],
            'street' => $parameters['street'],
            'number' => $parameters['number'],
            'zipcode' => $parameters['zipcode'],
            'city' => $parameters['city']
        ];
        if (!$this->validateParams($params)) {
            $_SESSION['error'] = "There was a problem validation fields";
            return false;
        }

        $sqlite = new SqliteOps($this->conn);
        $sqlite->insert($this->tableName, $params);

        header("Location: index.php?step=Step3");
    }

    /**
     * @param array $params
     * @return bool
     */
    public function validateParams(array $params) :bool
    {
        foreach($params as $i => $v) {
            if (empty($v)) {
                return false;
            }
        }

        return true;
    }
}
<?php

namespace Controller;

/**
 * Trait WunderTrait
 * @package Controller
 */
trait WunderTrait
{
    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getProgress()
    {
        return $this->progress;
    }
}
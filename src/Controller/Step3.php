<?php

namespace Controller;

use lib\DemoPayment;
use lib\SqliteOps;

require_once 'WunderTrait.php';

/**
 * Class Step3
 * @package Controller
 */
class Step3 extends Controller
{
    use WunderTrait;

    /**
     * @var \lib\Sqlite
     */
    private $conn;

    /**
     * @var string
     */
    private $view = 'step3';

    /**
     * @var string
     */
    private $name = 'Step 3';

    /**
     * @var int
     */
    private $progress = 75;


    /**
     * @var string
     */
    private $tableName = 'data_payment';

    /**
     * Step3 constructor.
     * @param $conn
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param $parameters
     * @return mixed|void
     */
    public function handlePost($parameters) {
        $params = [
            'customerId' => $_SESSION['customerId'],
            'owner' => $parameters['owner']
        ];

        if (!$this->validateParams($params)) {
            $_SESSION['error'] = "There was a problem validation fields";
            return false;
        }

        // Add IBAN and paymentDataId
        $params['iban'] = $parameters['iban'];
        $demoPayment = new DemoPayment();
        $dataPayment = $demoPayment->execute($params);
        if (is_array($dataPayment) && isset($dataPayment['error'])) {
            $_SESSION['error'] = $dataPayment['error'];
            return false;
        }
        $params['dataPaymentId'] = $dataPayment->paymentDataId;
        $_SESSION['dataPaymentId'] = $dataPayment->paymentDataId;

        $sqlite = new SqliteOps($this->conn);
        $sqlite->insert($this->tableName, $params);

        header("Location: index.php?step=Step4");
    }


    /**
     * @param array $params
     * @return bool
     */
    public function validateParams(array $params) :bool
    {
        foreach($params as $i => $v) {
            if (empty($v)) {
                return false;
            }
        }

        return true;
    }
}

<form action="index.php?step=Step3" method="POST">
    <div class="form-group">
        <?php
            if (isset($_SESSION['error'])) {
                echo '<p class="alert alert-danger">' . $_SESSION['error'] . '</p>';
                unset($_SESSION['error']);
            }
        ?>
    </div>
    <div class="form-group">
        <label for="owner">Account owner</label>
        <input type="text" name="owner" class="form-control" id="owner" maxlength="55" required>
    </div>
    <div class="form-group">
        <label for="iban">IBAN</label>
        <input type="text" name="iban" class="form-control" id="iban">
    </div>
    <input type="submit" class="btn btn-primary" value="Next step" />
</form>
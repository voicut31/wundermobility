<form action="index.php?step=Step1" method="POST">
    <div class="form-group">
        <?php
            if (isset($_SESSION['error'])) {
                echo '<p class="alert alert-danger">' . $_SESSION['error'] . '</p>';
                unset($_SESSION['error']);
            }
        ?>
    </div>
    <div class="form-group">
        <label for="firstname">Firstname</label>
        <input type="text" name="firstname" class="form-control" id="firstname" maxlength="55" required>
    </div>
    <div class="form-group">
        <label for="lastname">Lastname</label>
        <input type="text" name="lastname" class="form-control" id="lastname" maxlength="55" required>
    </div>
    <div class="form-group">
        <label for="telephone">Telephone</label>
        <input type="text" name="telephone" class="form-control" id="telephone" maxlength="55" required>
    </div>
    <input type="submit" class="btn btn-primary" value="Next step" />
</form>

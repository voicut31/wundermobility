<form action="index.php?step=Step2" method="POST">
    <div class="form-group">
        <?php
            if (isset($_SESSION['error'])) {
                echo '<p class="alert alert-danger">' . $_SESSION['error'] . '</p>';
                unset($_SESSION['error']);
            }
        ?>
    </div>
    <div class="form-group">
        <label for="street">Street</label>
        <input type="text" name="street" class="form-control" id="street" aria-describedby="Street" maxlength="100" required>
    </div>
    <div class="form-group">
        <label for="number">House number</label>
        <input type="text" name="number" class="form-control" id="number" aria-describedby="number" maxlength="20" required>
    </div>
    <div class="form-group">
        <label for="zipcode">Zip Code</label>
        <input type="text" name="zipcode" class="form-control" id="zipcode" aria-describedby="Zip Code" maxlength="100" required>
    </div>
    <div class="form-group">
        <label for="city">City</label>
        <input type="text" name="city" class="form-control" id="city" aria-describedby="City" maxlength="100" required>
    </div>
    <input type="submit" class="btn btn-primary" value="Next step" />
</form>

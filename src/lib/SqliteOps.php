<?php

namespace lib;

/**
 * Class SqliteConnection
 */
class SqliteOps
{
    /**
     * @var \SQLite3
     */
    private $conn;

    /**
     * SqliteOps constructor.
     * @param $conn
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param string $tableName
     * @param array $columns
     * @param array $values
     * @return int
     */
    public function insert(string $tableName, array $values = []) :int
    {
        $sql = 'INSERT INTO ' . $tableName . ' (' . implode(',',  array_keys($values)) . ') '
            . 'VALUES(:' . implode(', :', array_keys($values)) . ')';

            $stmt = $this->conn->prepare($sql);
            foreach ($values as $i => $v) {
                $stmt->bindValue(':' . $i, $v);
            }

            try{
                $stmt->execute();
            } catch(\Exception $e) {
                die ($e->getMessage());
            }

            return $this->conn->lastInsertRowID();
    }
}

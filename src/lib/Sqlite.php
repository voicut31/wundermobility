<?php

namespace lib;

/**
 * Class SqliteConnection
 */
class Sqlite
{
    // Hold the class instance.
    private static $instance = null;
    private $conn;

    // The db connection is established in the private constructor.
    private function __construct()
    {
        $this->conn = new \SQLite3(LOCATION);
    }

    /**
     * @return Sqlite|null
     */
    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new Sqlite();
        }

        return self::$instance;
    }

    /**
     * @return \SQLite3
     */
    public function getConnection()
    {
        return $this->conn;
    }
}

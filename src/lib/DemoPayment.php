<?php

namespace lib;

/**
 * Class DemoPayment
 * @package lib
 */
class DemoPayment
{
    /**
     * @param array $params
     * @return array|mixed
     */
    public function execute(array $params)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data");

        $payload = json_encode($params);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $content = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($responseCode !== 200) {
            return ['error' =>  $content];
        }

        curl_close($ch);

        $data = json_decode($content);

        return $data;
    }
}